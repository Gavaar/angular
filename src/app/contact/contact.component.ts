import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Feedback, ContactType } from '../shared/feedback';
import { FeedbackService } from '../services/feedback.service';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedbackcopy = null;
  contactType = ContactType;
  //booleans to show what should be shown (to be used in the onSubmit method)
  formvis = false;
  formpreview = true;
  loadingscreen = true;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required':      'First Name is required.',
      'minlength':     'First Name must be at least 2 characters long.',
      'maxlength':     'FirstName cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':      'Last Name is required.',
      'minlength':     'Last Name must be at least 2 characters long.',
      'maxlength':     'Last Name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':      'Tel. number is required.',
      'pattern':       'Tel. number must contain only numbers.'
    },
    'email': {
      'required':      'Email is required.',
      'email':         'Email not in valid format.'
    }
  };

  constructor(private fb: FormBuilder,
              private feedbackservice: FeedbackService) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm(): void {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      lastname: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(25)] ],
      telnum: ['', [Validators.required, Validators.pattern] ],
      email: ['', [Validators.required, Validators.email] ],
      agree: false,
      contacttype: 'None',
      message: ''
    });

    this.feedbackForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now

  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) {return;} //if void, return void
    const form = this.feedbackForm; //create constant form equal to current form input
    for (const field in this.formErrors) { //for EACH ITEM in the array formErrors (the one that saves the errors to be displayed)
      //clear previous error messages if any
      this.formErrors[field]='';
      const control = form.get(field); //create constant control (p/ field), which takes the value of the input on the field of form
      if (control && control.dirty && !control.valid) { //field has to be dirty, has to be invalid, has to have value input
        const messages = this.validationMessages[field]; //messages will be equal to validation Messages (error messages) in the field (all of them)
        for (const key in control.errors) { //control is the value in the input of the field of form. For EACH ITEM that is not validating (error) we create a key
          this.formErrors[field] += messages[key] + ' '; //we are adding to the alreadySelected field in formError a message for the key (already filtered)
        }
      }
    }
  }

  onSubmit() {
    this.feedbackcopy = this.feedbackForm.value;
    this.formvis = true; this.loadingscreen=false; //-form, +loading.sc
    console.log(this.feedbackcopy);
    this.feedbackservice.submitFeedback(this.feedbackcopy)
    //subscribing to the submit to show the different things
      .subscribe(feedback => {
        this.loadingscreen=true; this.formpreview=false; //-loading.sc, +preview
        setTimeout(() => {this.feedbackForm.reset({ //form reset
          firstname: '',
          lastname: '',
          telnum: '',
          email: '',
          agree: false,
          contacttype: 'None',
          message: ''
        }); 
      this.formpreview=true; this.formvis=false; //-preview, +form
      }, 5000); //after 5 seconds
    });
  }
}
