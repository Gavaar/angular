import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Dish } from "../shared/dish";
import { DishService } from '../services/dish.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { visibility, flyInOut, expand } from '../animations/app.animation';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishcopy = null;
  dishIds: number[];
  prev: number;
  next: number;
  errMess: string;
  visibility = 'shown';

  feedbackForm: FormGroup;
  //variables for the form
  formErrors = {
    'rating': '',
    'comment': '',
    'author': '',
  };

  validationMessages = {
    'comment': {
      'required': 'A Comment is required.',
    },
    'author': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 2 characters long.',
      'pattern': 'Name must contain only letters.'
    }
  };

  constructor(private dishservice: DishService,
              private route: ActivatedRoute,
              private location: Location,
              private fb: FormBuilder,
              @Inject('BaseURL') private BaseURL) { 
    this.createForm();
              }

  ngOnInit() {
    //Get Dish Ids on init.
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds, errmess => this.errMess = <any>errmess);
    //Get the Observable from the routelink (params), then use that to get the dish of that specific id. Also change values of prev and next.
    this.route.params
      .switchMap((params: Params) => {this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
      errmess => { this.dish = null; this.errMess = <any>errmess; });
  }

  onSubmit(){
    this.feedbackForm.value.date = new Date().toISOString();
    this.dishcopy.comments.push(this.feedbackForm.value); //push to the dishcopy null var
    this.dishcopy.save()  //save the dishcopy in the server (put or push)
      .subscribe(dish => {this.dish = dish; console.log(this.dish)}); //when dishcopy receives a response (param dish), the dish from this component will be equaled to that server dish.
    this.feedbackForm.reset({
        rating: '5',
        comment: '',
        author: '',
        date: ''
    });
  }

  createForm():void {
    this.feedbackForm = this.fb.group({
      rating: ['5'],
      comment: ['', Validators.required],
      author: ['', [Validators.required, Validators.minLength(2), Validators.pattern] ],
      date: ['']
    });

    this.feedbackForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) {return;} //if void, return void
    const form = this.feedbackForm; //create constant form equal to current form input
    for (const field in this.formErrors) { //for EACH ITEM in the array formErrors (the one that saves the errors to be displayed)
      //clear previous error messages if any
      this.formErrors[field]='';
      const control = form.get(field); //create constant control (p/ field), which takes the value of the input on the field of form
      if (control && control.dirty && !control.valid) { //field has to be dirty, has to be invalid, has to have value input
        const messages = this.validationMessages[field]; //messages will be equal to validation Messages (error messages) in the field (all of them)
        for (const key in control.errors) { //control is the value in the input of the field of form. For EACH ITEM that is not validating (error) we create a key
          this.formErrors[field] += messages[key] + ' '; //we are adding to the alreadySelected field in formError a message for the key (already filtered)
        }
      }
    }
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

}
