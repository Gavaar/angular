import { Injectable } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Feedback } from '../Shared/feedback';

@Injectable()
export class FeedbackService {

  feedback: Feedback;

  constructor(private restangular: Restangular) { }

  submitFeedback(feedback) {

    return this.restangular.all('feedback').post(feedback);
  }

}
