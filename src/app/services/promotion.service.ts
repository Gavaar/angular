import { Injectable, Inject } from '@angular/core';
import { RestangularModule, Restangular } from 'ngx-restangular';

import { Promotion } from '../Shared/promotion';
import { baseURL } from '../Shared/baseurl';

import { Observable } from 'rxjs/Observable';
  import 'rxjs/add/operator/map';
  import 'rxjs/add/observable/of';


@Injectable()
export class PromotionService {

  constructor( private restangular: Restangular,
              @Inject('BaseURL') private baseURL ) { }

  getPromotions(): Observable<Promotion[]> {
    return this.restangular.all('promotions').getList();
  }

  getPromotion(id: number): Observable<Promotion> {
    return this.restangular.one('promotions', id).get();
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.restangular.all('promotions').getList({featured: true})
      .map(prom => prom[0]);
  }

}
